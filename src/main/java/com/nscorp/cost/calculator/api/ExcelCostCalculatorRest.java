package com.nscorp.cost.calculator.api;

import com.nscorp.cost.calculator.dto.RequestData;
import com.nscorp.cost.calculator.exception.CustomRsponseEntityExceptionHandler;
import com.nscorp.cost.calculator.mapper.RequestMapper;
import com.nscorp.cost.calculator.service.CostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class ExcelCostCalculatorRest extends CustomRsponseEntityExceptionHandler {
    private static final String APPLICATION_JSON_VALUE = "application/json";
    @Autowired
    private CostService costService;
    @Autowired
    private RequestMapper mapper;

    @CrossOrigin
    @RequestMapping(value = "/calculate", method = RequestMethod.POST,
            consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> calculateAndSaveService(@RequestBody RequestData request) {
        try {
            return new ResponseEntity<>(costService.computeCost(mapper.map(request)), OK);
        } catch (ConstraintViolationException ex) {
            return handleError(ex);
        }
    }


}
