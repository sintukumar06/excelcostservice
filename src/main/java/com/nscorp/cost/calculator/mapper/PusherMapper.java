package com.nscorp.cost.calculator.mapper;

import com.nscorp.cost.calculator.dto.PusherData;
import com.nscorp.cost.calculator.model.PushersInfo;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PusherMapper {
//    PusherMapper MAPPER = Mappers.getMapper(PusherMapper.class);

    PusherData map(PushersInfo pusherInfo);

    List<PusherData> map(List<PushersInfo> pushersInfo);

    PushersInfo map(PusherData pusherData);
}
