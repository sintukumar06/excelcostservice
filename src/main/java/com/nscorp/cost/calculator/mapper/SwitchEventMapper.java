package com.nscorp.cost.calculator.mapper;

import com.nscorp.cost.calculator.dto.SwitchEventData;
import com.nscorp.cost.calculator.model.SwitchEvent;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SwitchEventMapper {
    SwitchEvent map(SwitchEventData switchEvent);

    List<SwitchEvent> map(List<SwitchEventData> switchEvents);
}
