package com.nscorp.cost.calculator.mapper;

import com.nscorp.cost.calculator.dto.UnitTrainData;
import com.nscorp.cost.calculator.model.UnitTrain;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UnitTrainMapper {
    UnitTrain map(UnitTrainData unitTrain);

    List<UnitTrain> map(List<UnitTrainData> unitTrains);
}
