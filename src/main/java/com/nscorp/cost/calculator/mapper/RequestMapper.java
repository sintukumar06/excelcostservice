package com.nscorp.cost.calculator.mapper;

import com.nscorp.cost.calculator.dto.RequestData;
import com.nscorp.cost.calculator.model.RequestInputs;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {PusherMapper.class, UnitTrainMapper.class, SwitchEventMapper.class})
public interface RequestMapper {
    RequestInputs map(RequestData requestData);
}
