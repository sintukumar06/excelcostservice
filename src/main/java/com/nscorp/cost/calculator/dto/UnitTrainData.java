package com.nscorp.cost.calculator.dto;

public class UnitTrainData {
    private String division;
    private float loadedMiles;
    private int numberOfLocomotives;
    private float locomotiveDaysOnline;
    private float carDaysOnline;
    private int numberOfTrainStarts;
    private float networkEconomicFactor;

    public UnitTrainData() {
    }

    public String getDivision() {
        return this.division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public float getLoadedMiles() {
        return this.loadedMiles;
    }

    public void setLoadedMiles(float loadedMiles) {
        this.loadedMiles = loadedMiles;
    }

    public int getNumberOfLocomotives() {
        return numberOfLocomotives;
    }

    public void setNumberOfLocomotives(int numberOfLocomotives) {
        this.numberOfLocomotives = numberOfLocomotives;
    }

    public float getLocomotiveDaysOnline() {
        return this.locomotiveDaysOnline;
    }

    public void setLocomotiveDaysOnline(float locomotiveDaysOnline) {
        this.locomotiveDaysOnline = locomotiveDaysOnline;
    }

    public float getCarDaysOnline() {
        return this.carDaysOnline;
    }

    public void setCarDaysOnline(float carDaysOnline) {
        this.carDaysOnline = carDaysOnline;
    }

    public int getNumberOfTrainStarts() {
        return this.numberOfTrainStarts;
    }

    public void setNumberOfTrainStarts(int numberOfTrainStarts) {
        this.numberOfTrainStarts = numberOfTrainStarts;
    }

    public float getNetworkEconomicFactor() {
        return this.networkEconomicFactor;
    }

    public void setNetworkEconomicFactor(float networkEconomicFactor) {
        this.networkEconomicFactor = networkEconomicFactor;
    }
}
