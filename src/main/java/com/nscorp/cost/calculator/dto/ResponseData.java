package com.nscorp.cost.calculator.dto;

import java.util.List;

public class ResponseData {
    private CostSummary vec;
    private CostSummary vrc;
    private CostSummary coalDumping;
    private List<PusherData> pusherData;
    private List<SummaryData> summaryData;

    @java.beans.ConstructorProperties({"vec", "vrc", "coalDumping", "pusherData", "summaryData"})
    public ResponseData(CostSummary vec, CostSummary vrc, CostSummary coalDumping, List<PusherData> pusherData, List<SummaryData> summaryData) {
        this.vec = vec;
        this.vrc = vrc;
        this.coalDumping = coalDumping;
        this.pusherData = pusherData;
        this.summaryData = summaryData;
    }

    public ResponseData() {
    }

    public static ResponseDataBuilder builder() {
        return new ResponseDataBuilder();
    }

    public CostSummary getVec() {
        return this.vec;
    }

    public void setVec(CostSummary vec) {
        this.vec = vec;
    }

    public CostSummary getVrc() {
        return this.vrc;
    }

    public void setVrc(CostSummary vrc) {
        this.vrc = vrc;
    }

    public CostSummary getCoalDumping() {
        return this.coalDumping;
    }

    public void setCoalDumping(CostSummary coalDumping) {
        this.coalDumping = coalDumping;
    }

    public List<PusherData> getPusherData() {
        return this.pusherData;
    }

    public void setPusherData(List<PusherData> pusherData) {
        this.pusherData = pusherData;
    }

    public List<SummaryData> getSummaryData() {
        return this.summaryData;
    }

    public void setSummaryData(List<SummaryData> summaryData) {
        this.summaryData = summaryData;
    }

    public String toString() {
        return "ResponseData(vec=" + this.getVec() + ", vrc=" + this.getVrc() + ", coalDumping=" + this.getCoalDumping() + ", pusherData=" + this.getPusherData() + ", summaryData=" + this.getSummaryData() + ")";
    }

    public static class ResponseDataBuilder {
        private CostSummary vec;
        private CostSummary vrc;
        private CostSummary coalDumping;
        private List<PusherData> pusherData;
        private List<SummaryData> summaryData;

        ResponseDataBuilder() {
        }

        public ResponseData.ResponseDataBuilder vec(CostSummary vec) {
            this.vec = vec;
            return this;
        }

        public ResponseData.ResponseDataBuilder vrc(CostSummary vrc) {
            this.vrc = vrc;
            return this;
        }

        public ResponseData.ResponseDataBuilder coalDumping(CostSummary coalDumping) {
            this.coalDumping = coalDumping;
            return this;
        }

        public ResponseData.ResponseDataBuilder pusherData(List<PusherData> pusherData) {
            this.pusherData = pusherData;
            return this;
        }

        public ResponseData.ResponseDataBuilder summaryData(List<SummaryData> summaryData) {
            this.summaryData = summaryData;
            return this;
        }

        public ResponseData build() {
            return new ResponseData(vec, vrc, coalDumping, pusherData, summaryData);
        }

        public String toString() {
            return "ResponseData.ResponseDataBuilder(vec=" + this.vec + ", vrc=" + this.vrc + ", coalDumping=" + this.coalDumping + ", pusherData=" + this.pusherData + ", summaryData=" + this.summaryData + ")";
        }
    }
}
