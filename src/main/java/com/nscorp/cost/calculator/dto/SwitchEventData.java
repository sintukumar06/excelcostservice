package com.nscorp.cost.calculator.dto;

public class SwitchEventData {
    private String switchType;
    private String cityAndState;

    public SwitchEventData() {
    }

    public String getSwitchType() {
        return this.switchType;
    }

    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }

    public String getCityAndState() {
        return this.cityAndState;
    }

    public void setCityAndState(String cityAndState) {
        this.cityAndState = cityAndState;
    }
}
