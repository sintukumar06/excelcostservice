package com.nscorp.cost.calculator.dto;

import com.nscorp.cost.calculator.model.UnitTrain;

import java.util.List;

public class RequestData {
    private String mktgMajorGroup;
    private int numberOfCars;
    private float ladingWeightPerCar;
    private String mktgCarType;
    private String carOwner;
    private float emptyReturnRatio;
    private String trainType;
    private String coalDumpingCity;
    private List<PusherData> pushers;
    private List<SwitchEventData> switchEvents;
    private ManualInput manualInput;
    private List<UnitTrain> unitTrains;
    private boolean carHiredOrDailyRate = true;
    private boolean avgJointFacilityOrHaulageOrLease = false;
    private boolean specializedFacilitySvcs = false;

    public RequestData() {
    }

    public String getMktgMajorGroup() {
        return this.mktgMajorGroup;
    }

    public void setMktgMajorGroup(String mktgMajorGroup) {
        this.mktgMajorGroup = mktgMajorGroup;
    }

    public int getNumberOfCars() {
        return this.numberOfCars;
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    public float getLadingWeightPerCar() {
        return this.ladingWeightPerCar;
    }

    public void setLadingWeightPerCar(float ladingWeightPerCar) {
        this.ladingWeightPerCar = ladingWeightPerCar;
    }

    public String getMktgCarType() {
        return this.mktgCarType;
    }

    public void setMktgCarType(String mktgCarType) {
        this.mktgCarType = mktgCarType;
    }

    public String getCarOwner() {
        return this.carOwner;
    }

    public void setCarOwner(String carOwner) {
        this.carOwner = carOwner;
    }

    public float getEmptyReturnRatio() {
        return this.emptyReturnRatio;
    }

    public void setEmptyReturnRatio(float emptyReturnRatio) {
        this.emptyReturnRatio = emptyReturnRatio;
    }

    public String getTrainType() {
        return this.trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public String getCoalDumpingCity() {
        return this.coalDumpingCity;
    }

    public void setCoalDumpingCity(String coalDumpingCity) {
        this.coalDumpingCity = coalDumpingCity;
    }

    public List<PusherData> getPushers() {
        return this.pushers;
    }

    public void setPushers(List<PusherData> pushers) {
        this.pushers = pushers;
    }

    public List<SwitchEventData> getSwitchEvents() {
        return this.switchEvents;
    }

    public void setSwitchEvents(List<SwitchEventData> switchEvents) {
        this.switchEvents = switchEvents;
    }

    public ManualInput getManualInput() {
        return this.manualInput;
    }

    public void setManualInput(ManualInput manualInput) {
        this.manualInput = manualInput;
    }

    public List<UnitTrain> getUnitTrains() {
        return this.unitTrains;
    }

    public void setUnitTrains(List<UnitTrain> unitTrains) {
        this.unitTrains = unitTrains;
    }

    public boolean isCarHiredOrDailyRate() {
        return this.carHiredOrDailyRate;
    }

    public void setCarHiredOrDailyRate(boolean carHiredOrDailyRate) {
        this.carHiredOrDailyRate = carHiredOrDailyRate;
    }

    public boolean isAvgJointFacilityOrHaulageOrLease() {
        return this.avgJointFacilityOrHaulageOrLease;
    }

    public void setAvgJointFacilityOrHaulageOrLease(boolean avgJointFacilityOrHaulageOrLease) {
        this.avgJointFacilityOrHaulageOrLease = avgJointFacilityOrHaulageOrLease;
    }

    public boolean isSpecializedFacilitySvcs() {
        return this.specializedFacilitySvcs;
    }

    public void setSpecializedFacilitySvcs(boolean specializedFacilitySvcs) {
        this.specializedFacilitySvcs = specializedFacilitySvcs;
    }
}
