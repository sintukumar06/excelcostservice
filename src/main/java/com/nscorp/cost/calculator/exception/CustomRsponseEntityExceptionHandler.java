package com.nscorp.cost.calculator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CustomRsponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleError(ConstraintViolationException ex) {
        return new ResponseEntity(getErrorBody(ex, HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    private ErrorDetails getErrorBody(ConstraintViolationException ex, HttpStatus status) {
        return ErrorDetails.builder().timestamp(new Date()).status(status.toString()).errors(getErrors(ex)).build();
    }

    private List<FieldError> getErrors(ConstraintViolationException ex) {
        return ex.getConstraintViolations().stream()
                .map(e -> FieldError.builder().field(e.getPropertyPath().toString()).defaultMessage(e.getMessage()).build())
                .collect(Collectors.toList());
    }

}
