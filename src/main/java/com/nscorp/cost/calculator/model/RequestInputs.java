package com.nscorp.cost.calculator.model;

import com.nscorp.cost.calculator.dto.ManualInput;
import com.nscorp.cost.calculator.validation.constraints.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class RequestInputs {
    @MajorGroupType
    private String mktgMajorGroup;
    @Min(value = 1)
    private int numberOfCars;
    private float ladingWeightPerCar;
    //private int numberOfLocomotives;
    @CarType
    private String mktgCarType;
    @CarOwner
    private String carOwner;
    private float emptyReturnRatio;
    @TrainType
    private String trainType;
    @City
    private String coalDumpingCity;
    @Valid
    private List<PushersInfo> pushers;
    @Valid
    private List<SwitchEvent> switchEvents;
    private ManualInput manualInput;
    @Valid
    @NotNull
    @NotEmpty
    private List<UnitTrain> unitTrains;
    private boolean carHiredOrDailyRate = true;
    private boolean avgJointFacilityOrHaulageOrLease = false;
    private boolean specializedFacilitySvcs = false;

    public RequestInputs(String mktgMajorGroup, int numberOfCars, float ladingWeightPerCar, String mktgCarType, String carOwner, float emptyReturnRatio, String trainType, String coalDumpingCity, List<PushersInfo> pushers, List<SwitchEvent> switchEvents, ManualInput manualInput, List<UnitTrain> unitTrains, boolean carHiredOrDailyRate, boolean avgJointFacilityOrHaulageOrLease, boolean specializedFacilitySvcs) {
        this.mktgMajorGroup = mktgMajorGroup;
        this.numberOfCars = numberOfCars;
        this.ladingWeightPerCar = ladingWeightPerCar;
        //this.numberOfLocomotives = numberOfLocomotives;
        this.mktgCarType = mktgCarType;
        this.carOwner = carOwner;
        this.emptyReturnRatio = emptyReturnRatio;
        this.trainType = trainType;
        this.coalDumpingCity = coalDumpingCity;
        this.pushers = pushers;
        this.switchEvents = switchEvents;
        this.manualInput = manualInput;
        this.unitTrains = unitTrains;
        this.carHiredOrDailyRate = carHiredOrDailyRate;
        this.avgJointFacilityOrHaulageOrLease = avgJointFacilityOrHaulageOrLease;
        this.specializedFacilitySvcs = specializedFacilitySvcs;
    }

    public RequestInputs() {
    }

    public static RequestInputsBuilder builder() {
        return new RequestInputsBuilder();
    }

    public String getMktgMajorGroup() {
        return this.mktgMajorGroup;
    }

    public int getNumberOfCars() {
        return this.numberOfCars;
    }

    public float getLadingWeightPerCar() {
        return this.ladingWeightPerCar;
    }

    public String getMktgCarType() {
        return this.mktgCarType;
    }

    public String getCarOwner() {
        return this.carOwner;
    }

    public float getEmptyReturnRatio() {
        return this.emptyReturnRatio;
    }

    public String getTrainType() {
        return this.trainType;
    }

    public String getCoalDumpingCity() {
        return this.coalDumpingCity;
    }

    public List<PushersInfo> getPushers() {
        return this.pushers;
    }

    public List<SwitchEvent> getSwitchEvents() {
        return this.switchEvents;
    }

    public ManualInput getManualInput() {
        return this.manualInput;
    }

    public List<UnitTrain> getUnitTrains() {
        return this.unitTrains;
    }

    public boolean isCarHiredOrDailyRate() {
        return this.carHiredOrDailyRate;
    }

    public void setCarHiredOrDailyRate(boolean carHiredOrDailyRate) {
        this.carHiredOrDailyRate = carHiredOrDailyRate;
    }

    public boolean isAvgJointFacilityOrHaulageOrLease() {
        return this.avgJointFacilityOrHaulageOrLease;
    }

    public void setAvgJointFacilityOrHaulageOrLease(boolean avgJointFacilityOrHaulageOrLease) {
        this.avgJointFacilityOrHaulageOrLease = avgJointFacilityOrHaulageOrLease;
    }

    public boolean isSpecializedFacilitySvcs() {
        return this.specializedFacilitySvcs;
    }

    public void setSpecializedFacilitySvcs(boolean specializedFacilitySvcs) {
        this.specializedFacilitySvcs = specializedFacilitySvcs;
    }

    public void setMktgMajorGroup(String mktgMajorGroup) {
        this.mktgMajorGroup = mktgMajorGroup;
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    public void setLadingWeightPerCar(float ladingWeightPerCar) {
        this.ladingWeightPerCar = ladingWeightPerCar;
    }

    public void setMktgCarType(String mktgCarType) {
        this.mktgCarType = mktgCarType;
    }

    public void setCarOwner(String carOwner) {
        this.carOwner = carOwner;
    }

    public void setEmptyReturnRatio(float emptyReturnRatio) {
        this.emptyReturnRatio = emptyReturnRatio;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public void setCoalDumpingCity(String coalDumpingCity) {
        this.coalDumpingCity = coalDumpingCity;
    }

    public void setUnitTrains(List<UnitTrain> unitTrains) {
        this.unitTrains = unitTrains;
    }

    public void setPushers(List<PushersInfo> pushers) {
        this.pushers = pushers;
    }

    public void setSwitchEvents(List<SwitchEvent> switchEvents) {
        this.switchEvents = switchEvents;
    }

    public void setManualInput(ManualInput manualInput) {
        this.manualInput = manualInput;
    }

    public static class RequestInputsBuilder {
        private String mktgMajorGroup;
        private int numberOfCars;
        private float ladingWeightPerCar;
        private String mktgCarType;
        private String carOwner;
        private float emptyReturnRatio;
        private String trainType;
        private String coalDumpingCity;
        private List<PushersInfo> pushers;
        private List<SwitchEvent> switchEvents;
        private ManualInput manualInput;
        private List<UnitTrain> unitTrains;
        private boolean carHiredOrDailyRate;
        private boolean avgJointFacilityOrHaulageOrLease;
        private boolean specializedFacilitySvcs;

        RequestInputsBuilder() {
        }

        public RequestInputs.RequestInputsBuilder mktgMajorGroup(String mktgMajorGroup) {
            this.mktgMajorGroup = mktgMajorGroup;
            return this;
        }

        public RequestInputs.RequestInputsBuilder numberOfCars(int numberOfCars) {
            this.numberOfCars = numberOfCars;
            return this;
        }

        public RequestInputs.RequestInputsBuilder ladingWeightPerCar(float ladingWeightPerCar) {
            this.ladingWeightPerCar = ladingWeightPerCar;
            return this;
        }

        public RequestInputs.RequestInputsBuilder mktgCarType(String mktgCarType) {
            this.mktgCarType = mktgCarType;
            return this;
        }

        public RequestInputs.RequestInputsBuilder carOwner(String carOwner) {
            this.carOwner = carOwner;
            return this;
        }

        public RequestInputs.RequestInputsBuilder emptyReturnRatio(float emptyReturnRatio) {
            this.emptyReturnRatio = emptyReturnRatio;
            return this;
        }

        public RequestInputs.RequestInputsBuilder trainType(String trainType) {
            this.trainType = trainType;
            return this;
        }

        public RequestInputs.RequestInputsBuilder coalDumpingCity(String coalDumpingCity) {
            this.coalDumpingCity = coalDumpingCity;
            return this;
        }

        public RequestInputs.RequestInputsBuilder pushers(List<PushersInfo> pushers) {
            this.pushers = pushers;
            return this;
        }

        public RequestInputs.RequestInputsBuilder switchEvents(List<SwitchEvent> switchEvents) {
            this.switchEvents = switchEvents;
            return this;
        }

        public RequestInputs.RequestInputsBuilder manualInput(ManualInput manualInput) {
            this.manualInput = manualInput;
            return this;
        }

        public RequestInputs.RequestInputsBuilder unitTrains(List<UnitTrain> unitTrains) {
            this.unitTrains = unitTrains;
            return this;
        }

        public RequestInputs.RequestInputsBuilder carHiredOrDailyRate(boolean carHiredOrDailyRate) {
            this.carHiredOrDailyRate = carHiredOrDailyRate;
            return this;
        }

        public RequestInputs.RequestInputsBuilder avgJointFacilityOrHaulageOrLease(boolean avgJointFacilityOrHaulageOrLease) {
            this.avgJointFacilityOrHaulageOrLease = avgJointFacilityOrHaulageOrLease;
            return this;
        }

        public RequestInputs.RequestInputsBuilder specializedFacilitySvcs(boolean specializedFacilitySvcs) {
            this.specializedFacilitySvcs = specializedFacilitySvcs;
            return this;
        }

        public RequestInputs build() {
            return new RequestInputs(mktgMajorGroup, numberOfCars, ladingWeightPerCar, mktgCarType, carOwner, emptyReturnRatio, trainType, coalDumpingCity, pushers, switchEvents, manualInput, unitTrains, carHiredOrDailyRate, avgJointFacilityOrHaulageOrLease, specializedFacilitySvcs);
        }

        public String toString() {
            return "RequestInputs.RequestInputsBuilder(mktgMajorGroup=" + this.mktgMajorGroup + ", numberOfCars=" + this.numberOfCars + ", ladingWeightPerCar=" + this.ladingWeightPerCar + ", mktgCarType=" + this.mktgCarType + ", carOwner=" + this.carOwner + ", emptyReturnRatio=" + this.emptyReturnRatio + ", trainType=" + this.trainType + ", coalDumpingCity=" + this.coalDumpingCity + ", pushers=" + this.pushers + ", switchEvents=" + this.switchEvents + ", manualInput=" + this.manualInput + ", unitTrains=" + this.unitTrains + ", carHiredOrDailyRate=" + this.carHiredOrDailyRate + ", avgJointFacilityOrHaulageOrLease=" + this.avgJointFacilityOrHaulageOrLease + ", specializedFacilitySvcs=" + this.specializedFacilitySvcs + ")";
        }
    }
}

