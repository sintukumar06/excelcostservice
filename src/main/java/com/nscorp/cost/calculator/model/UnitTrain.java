package com.nscorp.cost.calculator.model;

import com.nscorp.cost.calculator.validation.constraints.Division;

import javax.validation.constraints.NotNull;

public class UnitTrain {
    @NotNull
    @Division
    private String division;
    private float loadedMiles;
    private int numberOfLocomotives;
    private float locomotiveDaysOnline;
    private float carDaysOnline;
    private int numberOfTrainStarts;
    private float networkEconomicFactor;

    public UnitTrain(String division, float loadedMiles, int numberOfLocomotives, float locomotiveDaysOnline, float carDaysOnline, int numberOfTrainStarts, float networkEconomicFactor) {
        this.division = division;
        this.loadedMiles = loadedMiles;
        this.numberOfLocomotives = numberOfLocomotives;
        this.locomotiveDaysOnline = locomotiveDaysOnline;
        this.carDaysOnline = carDaysOnline;
        this.numberOfTrainStarts = numberOfTrainStarts;
        this.networkEconomicFactor = networkEconomicFactor;
    }

    public UnitTrain() {
    }

    public static UnitTrainBuilder builder() {
        return new UnitTrainBuilder();
    }

    public String getDivision() {
        return this.division;
    }

    public float getLoadedMiles() {
        return this.loadedMiles;
    }

    public int getNumberOfLocomotives() {
        return numberOfLocomotives;
    }

    public void setNumberOfLocomotives(int numberOfLocomotives) {
        this.numberOfLocomotives = numberOfLocomotives;
    }

    public float getLocomotiveDaysOnline() {
        return this.locomotiveDaysOnline;
    }

    public float getCarDaysOnline() {
        return this.carDaysOnline;
    }

    public int getNumberOfTrainStarts() {
        return this.numberOfTrainStarts;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setLoadedMiles(float loadedMiles) {
        this.loadedMiles = loadedMiles;
    }

    public void setLocomotiveDaysOnline(float locomotiveDaysOnline) {
        this.locomotiveDaysOnline = locomotiveDaysOnline;
    }

    public void setCarDaysOnline(float carDaysOnline) {
        this.carDaysOnline = carDaysOnline;
    }

    public void setNumberOfTrainStarts(int numberOfTrainStarts) {
        this.numberOfTrainStarts = numberOfTrainStarts;
    }

    public float getNetworkEconomicFactor() {
        return this.networkEconomicFactor;
    }

    public void setNetworkEconomicFactor(float networkEconomicFactor) {
        this.networkEconomicFactor = networkEconomicFactor;
    }

    public static class UnitTrainBuilder {
        private String division;
        private float loadedMiles;
        private int numberOfLocomotives;
        private float locomotiveDaysOnline;
        private float carDaysOnline;
        private int numberOfTrainStarts;
        private float networkEconomicFactor;

        UnitTrainBuilder() {
        }

        public UnitTrain.UnitTrainBuilder division(String division) {
            this.division = division;
            return this;
        }

        public UnitTrain.UnitTrainBuilder loadedMiles(float loadedMiles) {
            this.loadedMiles = loadedMiles;
            return this;
        }

        public UnitTrain.UnitTrainBuilder numberOfLocomotives(int numberOfLocomotives) {
            this.numberOfLocomotives = numberOfLocomotives;
            return this;
        }

        public UnitTrain.UnitTrainBuilder locomotiveDaysOnline(float locomotiveDaysOnline) {
            this.locomotiveDaysOnline = locomotiveDaysOnline;
            return this;
        }

        public UnitTrain.UnitTrainBuilder carDaysOnline(float carDaysOnline) {
            this.carDaysOnline = carDaysOnline;
            return this;
        }

        public UnitTrain.UnitTrainBuilder numberOfTrainStarts(int numberOfTrainStarts) {
            this.numberOfTrainStarts = numberOfTrainStarts;
            return this;
        }

        public UnitTrain.UnitTrainBuilder networkEconomicFactor(float networkEconomicFactor) {
            this.networkEconomicFactor = networkEconomicFactor;
            return this;
        }

        public UnitTrain build() {
            return new UnitTrain(division, loadedMiles, numberOfLocomotives, locomotiveDaysOnline, carDaysOnline, numberOfTrainStarts, networkEconomicFactor);
        }

        public String toString() {
            return "UnitTrain.UnitTrainBuilder(division=" + this.division + ", loadedMiles=" + this.loadedMiles + ", locomotiveDaysOnline=" + this.locomotiveDaysOnline + ", carDaysOnline=" + this.carDaysOnline + ", numberOfTrainStarts=" + this.numberOfTrainStarts + ", networkEconomicFactor=" + this.networkEconomicFactor + ")";
        }
    }
}

